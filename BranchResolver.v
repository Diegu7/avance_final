module BranchResolver(
	input beq,
	input bne,
	input bgez,
	input bgtz,
	input blez,
	input bltz,
	input zero,
	input sign,
	output bt
);

always@(*)begin
	if(beq) bt = zero;
	else if(bne) bt = ~zero;
	else if(bgez) bt = zero || ~sign;
	else if(bgtz) bt = ~sign;
	else if(blez) bt = zero || sign;
	else if(bltz) bt = sign;
	else bt = 1'b0;
end


endmodule
