`include "opcodes.vh" 

module ControlUnit(
    input [5:0] op, //! Opcode
    input [5:0] fn, //! Function
    input [4:0] rt, //! BGEZ = 1, BLTZ = 0
    output jmp, //! Jump signal
    output beq, //! BEQ signal
    output bne, //! BNE signal
    output memr, //! Memory Read
    output [1:0] m2r, //! Register File write data selection
    output szext, //! Sign/Zero Extension Selection
    output memw, //! Memory Write
    output [1:0] asrc, //! ALU source
    output regw, //! Register Write
    output [1:0] regd, //! Register Destination Address selection
    output [3:0] aop, //! ALU operation
    output iop, //! Invalid Opcode
    output mBE, //! Mem Bit Extension
    output [1:0] mdSize, //! Mem Data Size
    output shift, //! Shift Operation
    output bgez, //! Branch Greater or Equal to Zero
    output bgtz, //! Branch Greater Than Zero
    output blez, //! Branch Less or Equal to Zero
    output bltz, //! Branch Less Than Zero
    output jr, //! Jump to Register
    output jal //! Jump and Link
);

always @ (op or fn)
begin
    jmp = 1'b0;
    beq = 1'b0;
    bne = 1'b0;
    memr = 1'b0;
    m2r = 2'b0;
    memw = 1'b0;
    szext = 1'b1;
    asrc = 2'b0;
    regw = 1'b0;
    regd = 2'd0;
    aop = 4'b0000;
    mdSize = 2'b0;
    mBE = 1'b1;
    iop = 1'b1;

    shift = 1'b0;
    bgez = 1'b0;
    bgtz = 1'b0;
    blez = 1'b0;
    bltz = 1'b0;
    jr = 1'b0;
    jal = 1'b0;

    case (op)
        `NOP:
            case (fn)
            `ADD: 
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0000;
                    iop = 1'b0;
                end
            `ADDU:
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0000;
                    iop = 1'b0;
                end
            `SUBU:
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0001;
                    iop = 1'b0;
                end
            `SUB: 
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0001;
                    iop = 1'b0;
                end
            `AND: 
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0100;
                    iop = 1'b0;
                end
            `OR: 
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0011;
                    iop = 1'b0;
                end
            `SLT: 
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b0111;
                    iop = 1'b0;
                end
            `SLTU:
                begin
                    regw = 1'b1;
                    asrc = 2'b0;
                    regd = 2'd1;
                    m2r = 2'b0;
                    aop = 4'b1000;
                    iop = 1'b0;
                end
            `XOR:
                begin
                    regw = 1'b1;
                    regd = 2'd1;
                    aop = 4'b0101;
                    iop = 1'b0;
                end
            `JR: //thinking
                begin
                    jr = 1'b1;
                    iop = 1'b0;
                end
            `SLL: 
                begin
                    shift = 1'b1;
                    asrc = 2'd2;
                    regd = 2'd1;
                    regw = 1'b1;
                    aop = 4'b1001;
                    iop = 1'b0;
                end
            `SLLV:
                begin
                    shift = 1'b1;
                    asrc = 2'd3;
                    regw = 1'b1;
                    regd = 2'd1;
                    aop = 4'b1001;
                    iop = 1'b0;
                end
            `SRA:
                begin //maybe
                    shift = 1'b1;
                    asrc = 2'd2;
                    regd = 2'd1;
                    regw = 1'b1;
                    aop = 4'b1011;
                    iop = 1'b0;
                end
            `SRAV:
                begin //maybe2
                    shift = 1'b1;
                    asrc = 2'd3;
                    regd = 2'd1;
                    regw = 1'b1;
                    aop = 4'b1011;
                    iop = 1'b0;
                end
            `SRL:
                begin
                    shift = 1'b1;
                    asrc = 2'd2;
                    regd = 2'd1;
                    regw = 1'b1;
                    aop = 4'b1010;
                    iop = 1'b0;
                end
            `SRLV:
                begin
                    shift = 1'b1;
                    asrc = 2'd3;
                    regd = 2'd1;
                    regw = 1'b1;
                    aop = 4'b1010;
                    iop = 1'b0;
                end
            default: begin
            end
            endcase
        `ADDI: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `ADDIU: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `ORI: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b0011;
                szext = 1'b0;
                iop = 1'b0;
            end
        `ANDI: 
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b0100;
                szext = 1'b0;
                iop = 1'b0;
            end
        `XORI:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b0101;
                szext = 1'b0;
                iop = 1'b0;
            end
        `SLTI:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b0111;
                iop = 1'b0;
            end
        `SLTIU:
            begin
                regw = 1'b1;
                asrc = 2'b1;
                regd = 2'd0;
                m2r = 2'b0;
                aop = 4'b1000;
                iop = 1'b0;
            end
        `LW:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `LUI:
            begin
                regw = 1'b1;
                asrc = 2'b0;
                regd = 2'd0;
                m2r = 2'd2;
                iop = 1'b0;
            end
        `LB:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mdSize = 2'd2;
                iop = 1'b0;
            end
        `LBU:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mdSize = 2'd2;
                mBE = 1'b0;
                iop = 1'b0;
            end
        `LH:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mdSize = 2'b1;
                iop = 1'b0;
            end
        `LHU:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                regw = 1'b1;
                memr = 1'b1;
                m2r = 2'b1;
                aop = 4'b0000;
                mdSize = 2'b1;
                mBE = 1'b0;
                iop = 1'b0;
            end
        `SB:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                memw = 1'b1;
                aop = 4'b0000;
                mdSize = 2'd2;
                iop = 1'b0;
            end
        `SH:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                memw = 1'b1;
                aop = 4'b0000;
                mdSize = 2'b1;
                iop = 1'b0;
            end
        `SW:
            begin
                asrc = 2'b1;
                regd = 2'd0;
                memw = 1'b1;
                aop = 4'b0000;
                iop = 1'b0;
            end
        `BEQ:
            begin
              beq = 1'b1;
              aop = 4'b0001;
              iop = 1'b0;
            end
        `BNE:
            begin
                bne = 1'b1;
                aop = 4'b0001;
                iop = 1'b0;
            end
        `BGEZ:
            begin
                bgez = rt[0];
                bltz = ~rt[0];
                aop = 4'b0001;
                iop = 1'b0;
            end
        `BGTZ:
            begin
                bgtz = 1'b1;
                iop = 1'b0;
                aop = 4'b0001;
            end
        `BLEZ: 
            begin
                blez = 1'b1;
                iop = 1'b0;
                aop = 4'b0001;
            end
        `JUMP:
            begin
                jmp = 1'b1;
                iop = 1'b0;
            end
        `JAL:
            begin
                m2r = 2'd3;
                jal = 1'b1;
                iop = 1'b0;
                regd = 2'd2;
                regw = 1'd1;
            end
        default: begin
        end
    endcase
end

endmodule

