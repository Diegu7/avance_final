module DataDecoder(
	input [31:0] inD,
	input [1:0] ofs,
	input bitX,
	input [1:0] ds,
	output [31:0] oD
);
	
always@(ofs or bitX or ds)begin
	case(ds)
		2'd0:begin 
			oD = inD;
		end
		2'd1:begin
			if(ofs <= 2'b1) oD = {bitX ? {16{inD[31]}} : {16{1'b0}}, inD[31:16]};
			else oD = {bitX ? {16{inD[15]}} : {16{1'b0}}, inD[15:0]};
		end
		2'd2:begin
			case(ofs)
				2'b0: oD = {bitX ? {24{inD[31]}} : {24{1'b0}}, inD[31:24]};
				2'b1: oD = {bitX ? {24{inD[23]}} : {24{1'b0}}, inD[23:16]};
				2'd2: oD = {bitX ? {24{inD[15]}} : {24{1'b0}}, inD[15:8]};
				2'd3: oD = {bitX ? {24{inD[7]}} : {24{1'b0}}, inD[7:0]};
			endcase
		end
		default: oD = 32'dX;
	endcase
end

endmodule