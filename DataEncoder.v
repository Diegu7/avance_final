module DataEncoder(
	input [31:0] inD,
	input [1:0] ofs,
	input iwe,
	input [1:0] ds,
	output [31:0] oD,
	output [3:0] owe
);
	
always@(*)begin
	if(iwe) begin
		case(ds)
			2'd0:begin 
				oD = inD;
				owe = 4'b1111;
			end
			2'd1:begin
				if(ofs <= 2'b1) begin
					oD = {inD[15:0], {16{1'b0}}};
					owe = 4'b0011;
				end
				else begin 
					oD = {{16{1'b0}}, inD[15:0]};
					owe = 4'b1100;
				end
			end
			2'd2:begin
				case(ofs)
					2'b0: begin
						oD = {inD[7:0], {24{1'b0}}};
						owe = 4'b0001;
					end
					2'b1: begin
						oD = {{8{1'b0}}, inD[7:0], {16{1'b0}}};
						owe = 4'b0010;
					end					
					2'd2: begin
						oD = {{16{1'b0}}, inD[7:0], {8{1'b0}}};
						owe = 4'b0100;
					end
					2'd3: begin
						oD = {{24{1'b0}}, inD[7:0]};
						owe = 4'b1000;
					end
				endcase
			end
			default: begin
				oD = 32'dX;
				owe = 4'bX;
			end
		endcase
	end
	else begin
		oD = 32'd0;
		owe = 4'd0;
	end
end

endmodule