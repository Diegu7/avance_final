module IOMem(
	input [10:0] addr,
	input [7:0] keys,
	input [31:0] ms,
	input en,
	output [31:0] data
);

always@(*)begin
	if(en) begin
		case(addr)
			11'd1: data = {keys, 24'd0};
			11'd2: data = ms;
			default: data = 32'd0;
		endcase
	end
end

endmodule