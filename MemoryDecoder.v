module MemoryDecoder (
    input [31:0] in,
    input mW,
    input mR,
    output [2:0] mE,
    output [1:0] mB,
    output ima,
    output [12:0] out
);

//assign out = in[11:0];

always@(in)begin
	ima = 1'b0;
	if(in >= 32'h10010000 && in < 32'h10011000)begin
		ima = 1'b0;
		out = in[12:0];
		mB = 2'b00;
		if(mW || mR) mE = 3'b001;
		else mE = 3'b000;
	end
	else if(in >= 32'h7FFFEFFC && in < 32'h7FFFFFFC)begin
		ima = 1'b0;
		out = in[12:0] - 13'hEFFC + 13'h1000;
		mB = 2'b00;
		if(mW || mR) mE = 3'b001;
		else mE = 3'b000;
	end
	else if(in >= 32'hB800 && in <= 32'hCABF)begin
		ima = 1'b0;
		out = in[12:0] - 13'hB800;	//Not sure
		mB = 2'b01;
		if(mW || mR) mE = 3'b010;
		else mE = 3'b000;
	end
	else if(in >= 32'hFFFF0000 && in <= 32'hFFFF000F)begin //SHOULD BE 32'hFFFF000C
		ima = 1'b0;
		out = in[12:0]; //To be modified
		mB = 2'b10;
		if(mW || mR) mE = 3'b100;
		else mE = 3'b000;
	end
	else begin
		if(mW || mR) ima = 1'b1;
		out = 13'hX;
		mE = 3'hX;
		mB = 2'hX;
	end
end


endmodule

