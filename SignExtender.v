module SignExtender (
    input [15:0] in,
    input szext,
    output [31:0] out
);

    assign out = {szext ? {16{in[15]}} : {16{1'b0}}, in};
endmodule

