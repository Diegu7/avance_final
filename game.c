#include "game.h"
#include "screen.h"

int32_t posx1;
int32_t posy1;
int32_t height1;
int32_t width1;
int32_t vely1;
int32_t accy1;
int32_t velx1;
int32_t accx1;
int32_t state1;
int8_t stateChange1;


int32_t posx2;
int32_t posy2;
int32_t height2;
int32_t width2;
int32_t vely2;
int32_t accy2;
int32_t velx2;
int32_t accx2;
int32_t state2;
int8_t stateChange2;


int8_t result;

static const char p1[4][3] = {
	{' ', '0', ' '},
	{'-', '|', '-'},
	{' ', '|', ' '},
	{'/', ' ', '\\'}
};

static const char p1a[4][3] = {
	{' ', '\\', '0'},
	{'-', ' ', '|'},
	{' ', '/', ' '},
	{'/', '|', ' '}
};

static const char p2[4][3] = {
	{' ', '0', ' '},
	{'-', '|', '-'},
	{' ', '|', ' '},
	{'/', ' ', '\\'}
};

static const char p2a[4][3] = {
	{'0', '/', ' '},
	{'|', ' ', '-'},
	{' ', '\\', ' '},
	{' ', '|', '\\'}
};

static const char win[4][3] = {
	{'\\', '0', '/'},
	{' ', '|', ' '},
	{' ', '|', ' '},
	{'/', ' ', '\\'}
};

static const char ded[4][3] = {
	{' ', ' ', ' '},
	{' ', ' ', ' '},
	{'0', '-', '\\'},
	{'/', '|', '\\'}
};

void gameInit(){
	keypad_init();
	posx1 = 70;
	posy1 = 26;
	height1 = 4;
	width1 = 3;
	accy1 = 1;
	vely1 = 0;
	accx1 = 0;
	velx1 = 0;
	state1 = 0;
	stateChange1 = 1;

	posx2 = 10;
	posy2 = 26;
	height2 = 4;
	width2 = 3;
	accy2 = 1;
	vely2 = 0;
	accx2 = 0;
	velx2 = 0;
	state2 = 0;
	stateChange2 = 1;

	result = 0;
}

void gamePhysics(){
	if(result != 0)
		return;
	if(posy1 >= 25 && (state1 == 2 || state1 == 3)){ 
		state1 = 0;
		stateChange1 = 1;
	}
	if(stateChange1 == 1){
		if(state1 == 0){
			accy1 = 0;
			vely1 = 0;
			velx1 = 0;
			accx1 = 0;
			posy1 = 25;
			stateChange1 = 0;
		}
		else if(state1 == 1){
			vely1 = 0;
			accy1 = -1;
			velx1 = 0;
			accx1 = 0;
			stateChange1 = 0;
		}
		else if(state1 == 2){
			vely1 = 0;
			accy1 = 1;
			velx1 = 0;
			accx1 = 0;
			stateChange1 = 0;
		}
		else if(state1 == 3){
			vely1 = -1;
			accy1 = 1;
			velx1 = 0;
			accx1 = -1;
			stateChange1 = 0;
			//posy1 = posy1 - 1;
		}
	}
	if(posy1 >= 10){
		vely1 = vely1 + accy1;
		posy1 = posy1 + vely1;
	}
	else{
		state1 = 2;
		posy1 = 11;
		stateChange1 = 1;
	}
	if(posx1 >= 10){
		velx1 = velx1 + accx1;
		posx1 = posx1 + velx1;
	}


	if(posy2 >= 25 && (state2 == 2 || state2 == 3)){ 
		state2 = 0;
		stateChange2 = 1;
	}
	if(stateChange2 == 1){
		if(state2 == 0){
			accy2 = 0;
			vely2 = 0;
			velx2 = 0;
			accx2 = 0;
			posy2 = 25;
			stateChange2 = 0;
		}
		else if(state2 == 1){
			vely2 = 0;
			accy2 = -1;
			velx2 = 0;
			accx2 = 0;
			stateChange2 = 0;
		}
		else if(state2 == 2){
			vely2 = 0;
			accy2 = 1;
			velx2 = 0;
			accx2 = 0;
			stateChange2 = 0;
		}
		else if(state2 == 3){
			vely2 = -1;
			accy2 = 1;
			velx2 = 0;
			accx2 = 1;
			stateChange2 = 0;
			//posy1 = posy1 - 1;
		}
	}
	if(posy2 >= 10){
		vely2 = vely2 + accy2;
		posy2 = posy2 + vely2;
	}
	else{
		state2 = 2;
		posy2 = 11;
		stateChange2 = 1;
	}
	if(posx2 <= 70){
		velx2 = velx2 + accx2;
		posx2 = posx2 + velx2;
	}
	if(posx2 + width2 >= posx1){ 
		result = posy1 >= posy2 ? 1 : 2;
		state1 = 4;
		state2 = 4;
	}
}

void winCon(){

	set_cursor(0,0);
	puts(" Player 1                                                              Player 2 ");
	puts(" ------------------------------                   ------------------------------");
	if(result == 0)
		puts(" |============================|                   |============================|");
	else if(result == 2)
		puts(" |                            |                   |============================|");
	else
		puts(" |============================|                   |                            |");
	puts(" ------------------------------                   ------------------------------");
	if(result == 0)
		return;
	set_cursor(14, 30);
	puts("The winner is Player ");
	char res = result == 1 ? '1' : '2';
	put_char(res);
}

void render(){
	clear_screen();
	if(state1 == 0 || state1 == 1 || state1 == 2){
		for(int i = posy1, y = 0; i < posy1 + height1; i++, y++){
			for(int j = posx1, x = 0; j < posx1 + width1; j++, x++){
				set_cursor(i, j);
				put_char(p1[y][x]);
			}
		}
	}
	else if(state1 == 3){
		for(int i = posy1, y = 0; i < posy1 + height1; i++, y++){
			for(int j = posx1, x = 0; j < posx1 + width1; j++, x++){
				set_cursor(i, j);
				put_char(p1a[y][x]);
			}
		}
	}
	else if(state1 == 4){
		for(int i = posy1, y = 0; i < posy1 + height1; i++, y++){
			for(int j = posx1, x = 0; j < posx1 + width1; j++, x++){
				set_cursor(i, j);
				if(result == 2)
					put_char(win[y][x]);
				else
					put_char(ded[y][x]);
			}
		}
	}


	if(state2 == 0 || state2 == 1 || state2 == 2){
		for(int i = posy2, y = 0; i < posy2 + height2; i++, y++){
			for(int j = posx2, x = 0; j < posx2 + width2; j++, x++){
				set_cursor(i, j);
				put_char(p2[y][x]);
			}
		}
	}
	else if(state2 == 3){
		for(int i = posy2, y = 0; i < posy2 + height2; i++, y++){
			for(int j = posx2, x = 0; j < posx2 + width2; j++, x++){
				set_cursor(i, j);
				put_char(p2a[y][x]);
			}
		}
	}
	else if(state2 == 4){
		for(int i = posy2, y = 0; i < posy2 + height2; i++, y++){
			for(int j = posx2, x = 0; j < posx2 + width2; j++, x++){
				set_cursor(i, j);
				if(result == 1)
					put_char(win[y][x]);
				else
					put_char(ded[y][x]);
			}
		}
	}
}

void gameLoop(){
	gameInit();
	while(true){
    	gamePhysics();
    	render();
    	winCon();
    	delay_ms(100);//50
    	uint8_t key = keypad_getkey();
    	switch(key){
    		case 1://L
    			if(state1 == 1 || state1 == 2){
    				state1 = 3;
    				stateChange1 = 1;
    			}
    			break;

    		case 2://R
    			if(state2 == 1 || state2 == 2){
    				state2 = 3;
    				stateChange2 = 1;
    			}
    			break;

    		case 3://D
    			if(state2 == 0){
	    			state2 = 1;
	    			stateChange2 = 1;
    			}
    			break;

    		case 4://U
    			if(state1 == 0){
	    			state1 = 1;
	    			stateChange1 = 1;
    			}
    			break;

    		case 5://Q
    			
    			break;

    		case 6://P
    			
    			break;

    		case 7://B
    			break;

    		case 8://S
    			break;
    	}

	}
}