#ifndef _GAME_H
#define _GAME_H


#ifndef __ASSEMBLER__
#include "system.h"
#include "screen.h"
#include "keypad.h"




void gameInit();
void gameLoop();
void gamePhysics();
void render();
void winCon();

#endif
#endif