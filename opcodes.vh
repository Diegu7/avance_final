`define ADD 6'h20
`define ADDU 6'h21
`define ADDI 6'h08
`define ADDIU 6'h09
`define SUB 6'h22
`define AND 6'h24
`define ANDI 6'h0C
`define OR 6'h25
`define ORI 6'h0D
`define SLT 6'h2A
`define LW 6'h23
`define SW 6'h2B
`define BEQ 6'h04
`define BNE 6'h05
`define JUMP 6'h02
`define LUI 6'h0F

`define LB 6'h20
`define LBU 6'h24
`define LH 6'h21
`define LHU 6'h25
`define SH 6'h29
`define SB 6'h28


`define BGEZ 6'h01
`define BGTZ 6'h07
`define BLEZ 6'h06
`define BLTZ 6'h01
`define NOP 6'h00
`define SLL 6'h00
`define SRL 6'h02
`define SLLV 6'h04
`define SRLV 6'h06

`define SRA 6'h03
`define SRAV 6'h07
`define XOR 6'h26
`define XORI 6'h0E
`define SLTU 6'h2B
`define SLTI 6'h0A
`define SLTIU 6'h0B
`define JAL 6'h03
`define JR 6'h08


`define SUBU 6'h23